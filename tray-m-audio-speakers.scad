include <libtray.scad>;
include <libopenscad/mcube.scad>;
include <libopenscad/mcylinder.scad>;

target_width = 25.4 * 5.75;
target_depth = 25.4 * 6.25; // includes cord 
target_height = 25.4 * 6.25; 

front_wall_height = 1 * 0.25;
back_wall_height = 6;
corner_radius = 4;

padding = plate_thickness * 1;

rails = 4;

num_hanger_holes = 3;

cord_hole = true;
hanger_holes = true;

//debug_slug = true;
//debug_slice = true;

difference() {
    intersection() {
        union() {
            color("violet") 
                tray(
                    front_wall_height = base_size * front_wall_height,
                    back_wall_height =  base_size * back_wall_height,
                    depth = target_depth + padding, 
                    width = target_width + padding, 
                    interior_dimensions = true,

                    corner_radius = corner_radius,
                    rails = 0

                    );
        }

        if(!is_undef(debug_slice)) {
            translate([0, 50, 0]) {
                mcube([target_width + 100, target_depth + 100, 5], align = [0, -1, 3]);
            }
        }
    }
//}
    union() {
        
        
        
            if(!is_undef(hanger_holes) && hanger_holes == true) {
                translate([0, 0, target_height * 0.75]) {

                    for(x = [0 : num_hanger_holes - 1]) {
                        echo(x, x - ((num_hanger_holes - 1) / 2));
                        foo = x - ((num_hanger_holes - 1) / 2);
                        echo(foo);
                        bar = ((foo * target_width / (num_hanger_holes + 1)));
                        echo(bar);

translate([bar, 0, 0])
                    
                    
                    color("red") {
                        rotate([90, 0, 0]) {
                            mcylinder(d=4, h=50, center = true, $fn = 90);
                        }
                    }
                }

                }
                
            }





    if(!is_undef(cord_hole) && cord_hole == true) {
//        translate([base_size * 1.5, -(plate_thickness + base_size * 1.5 / 2), plate_thickness / 2]) {
//            mcylinder(d = base_size * 1.5, h = plate_thickness * 2, center = true, $fn = 100);
          translate([0, -(plate_thickness * 1.25 + 0.01 + base_size * 1.5 / 2), plate_thickness / 2]) {
              intersection() {
                  cylinder(h = plate_thickness * 2, d = base_size * 1.5 + plate_thickness / 2, center = true, $fn = 100);

          union() {

                // spool-shaped plug hole cutout for chamfered edges
                color("lightblue") {
                    mcylinder(d = base_size * 1.5, h = plate_thickness, $fn = 100, center = true);
                }
                color("lightgreen") {
                    translate([0, 0, plate_thickness * 3/4]) {
                        mcylinder(d1 = base_size * 1.5, d2 = base_size * 1.5 + (plate_thickness * 2), h = plate_thickness, center = true, $fn = 100);
                    }
                }
                color("orange") {
                    translate([0, 0, -plate_thickness * 3/4]) {
                        mcylinder(d2 = base_size * 1.5, d1 = base_size * 1.5 + (plate_thickness * 2), h = plate_thickness, center = true, $fn = 100);
                    }
                }
            }
        }
        }
    }
}
}

