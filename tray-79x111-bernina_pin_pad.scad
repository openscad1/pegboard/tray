include <libtray.scad>;
include <libopenscad/mcube.scad>;


module clover_clip_box_slug() {
    color("red") {
        translate([0, -plate_thickness * 1.5, plate_thickness]) {
            mcube([125, 76, 36], align = [0, -1, 1]);
        }
    }
}

//debug_slug = true;
//debug_ring = true;

intersection() {
    tray(
        front_wall_height = base_size * 1,
        back_wall_height =  base_size * 2,
        depth = 79, 
        width = 111, 
        interior_dimensions = true,
        //fn = 20,
        corner_radius = 35
    );

    if(!is_undef(debug_ring)) {
        translate([0, 0, base_size * 0.5])
        color("red") mcube([200, 200, plate_thickness], center = true, align = [0, -1, 0]);
    }

}