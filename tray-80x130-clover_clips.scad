include <libtray.scad>;
include <libopenscad/mcube.scad>;


module clover_clip_box_slug() {
    color("red") {
        translate([0, -plate_thickness * 1.5, plate_thickness]) {
            mcube([125, 76, 36], align = [0, -1, 1]);
        }
    }
}


//color("violet") 
    tray(
        front_wall_height = base_size * 3/4,
        back_wall_height =  base_size * 2,
        depth = 80, 
        width = 130, 
        interior_dimensions = true,

        corner_radius = 5
    );


//debug_slug = true;

if(!is_undef(debug_slug)) {
    clover_clip_box_slug();
}
