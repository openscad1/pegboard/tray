# libopenscad


## home, sweet home

    git@gitlab.com:m5658/libopenscad.git


## adopt me

    git subtree add  --message 'mtools git subtree add locator - libopenscad\nrepo=git@gitlab.com:m5658/libopenscad.git\nprefix=libopenscad\nbranch=main' --prefix libopenscad  git@gitlab.com:m5658/libopenscad.git main --squash


## convenience `make` targets

    echo 'all : openscad.all' >> Makefile
    echo 'clean : openscad.clean' >> Makefile
    echo 'tidy : openscad.tidy' >> Makefile
    git add Makefile
    git commit -m 'set default make targets' Makefile


## convenience values for .gitignore

    echo 'openscad.artifacts' >> .gitignore
    echo 'openscad.cache' >> .gitignore
    git add .gitignore
    git commit -m 'ignore openscad artifacts' .gitignore


## inherit updates from the mother ship

    git subtree pull --prefix libopenscad git@gitlab.com:m5658/libopenscad.git main --squash


## push our updates to the mother ship

    git subtree push --prefix libopenscad git@gitlab.com:m5658/libopenscad.git main --squash


