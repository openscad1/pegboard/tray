include <libtray.scad>;
include <libopenscad/mcube.scad>;
include <libopenscad/mcylinder.scad>;

target_width = 114;
target_depth = 152; // includes cord 
target_height = 80; 

front_wall_height = 1;
back_wall_height = 3;
corner_radius = 14;

padding = plate_thickness * 1;

rails = 4;

cord_hole = true;

//debug_slug = true;
//debug_slice = true;

difference() {
    intersection() {
        union() {
            color("violet") 
                tray(
                    front_wall_height = base_size * front_wall_height,
                    back_wall_height =  base_size * back_wall_height,
                    depth = target_depth + padding, 
                    width = target_width + padding, 
                    interior_dimensions = true,

                    corner_radius = corner_radius,
                    rails = rails

                    );
        }

        if(!is_undef(debug_slice)) {
            translate([0, 50, 0]) {
                mcube([target_width + 100, target_depth + 100, 5], align = [0, -1, 5]);
            }
        }
    }
//}
    if(!is_undef(cord_hole) && cord_hole == true) {
//        translate([base_size * 1.5, -(plate_thickness + base_size * 1.5 / 2), plate_thickness / 2]) {
//            mcylinder(d = base_size * 1.5, h = plate_thickness * 2, center = true, $fn = 100);
          translate([0, -(plate_thickness * 1.25 + 0.01 + base_size * 1.5 / 2), plate_thickness / 2]) {
              intersection() {
                  cylinder(h = plate_thickness * 2, d = base_size * 1.5 + plate_thickness / 2, center = true, $fn = 100);

          union() {

                // spool-shaped plug hole cutout for chamfered edges
                color("lightblue") {
                    mcylinder(d = base_size * 1.5, h = plate_thickness, $fn = 100, center = true);
                }
                color("lightgreen") {
                    translate([0, 0, plate_thickness * 3/4]) {
                        mcylinder(d1 = base_size * 1.5, d2 = base_size * 1.5 + (plate_thickness * 2), h = plate_thickness, center = true, $fn = 100);
                    }
                }
                color("orange") {
                    translate([0, 0, -plate_thickness * 3/4]) {
                        mcylinder(d2 = base_size * 1.5, d1 = base_size * 1.5 + (plate_thickness * 2), h = plate_thickness, center = true, $fn = 100);
                    }
                }
            }
        }
        }
    }
}

