include <libtray.scad>;
include <libopenscad/mcube.scad>;

target_width = 128;
target_depth = 140; // includes cord height
target_height = 80; 

front_wall_height = 1;
back_wall_height = 3;
corner_radius = 14;

padding = plate_thickness * 1;

rails = 4;

cord_hole = true;

//debug_slug = true;
//debug_slice = true;

difference() {
    intersection() {
        union() {
            color("violet") 
                tray(
                    front_wall_height = base_size * front_wall_height,
                    back_wall_height =  base_size * back_wall_height,
                    depth = target_depth + padding, 
                    width = target_width + padding, 
                    interior_dimensions = true,

                    corner_radius = corner_radius,
                    rails = rails

                    );
        }

        if(!is_undef(debug_slice)) {
            translate([0, 50, 0]) {
                mcube([target_width + 100, target_depth + 100, 5], align = [0, -1, 5]);
            }
        }
    }

    if(cord_hole) {
        translate([0, -(plate_thickness + base_size * 1.5 / 2), 0]) cylinder(d = base_size * 1.5, h = 20, center = true, $fn = 100);
    }
}