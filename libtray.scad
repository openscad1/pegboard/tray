include <libopenscad/pegboard-defaults.scad>;
include <libopenscad/pegboard-rail.scad>;
include <libopenscad/mbox.scad>;
include <libopenscad/mcube.scad>;

// NOSTL

//debug_model = true;
//debug_slug = true;

module slug() {
    color("red") {
        translate([0, -(plate_thickness + padding * 0.5), plate_thickness]) {
            mcube([target_width, target_depth, target_height], align = [0, -1, 1]);
        }
    }
}





module tray(front_wall_height, back_wall_height, depth, width, corner_radius, fn = 100, interior_dimensions = false, exterior_dimensions = false, rails = 3) {

    $fn = fn;
    
    msg = "must specify exactly one of 'interior_dimensions = true' or 'exterior_dimensions = true'";

    assert(interior_dimensions || exterior_dimensions, msg);
    assert(!(interior_dimensions && exterior_dimensions), msg);

    f_depth = interior_dimensions ? depth + plate_thickness * 2 : depth;
    f_width = interior_dimensions ? width + plate_thickness * 2 : width;
    
    difference() {
        union() {
            // back wall
            intersection() {
                radiused_box(
                    inside_length = f_depth - plate_thickness * 2,
                    inside_width = f_width,
                    inside_height = back_wall_height,
                    inside_radius = corner_radius,
                    wall_thickness = plate_thickness,
                    align = [0, -1, 1]
                );

                color("red") mcube([f_width, corner_radius + plate_thickness, back_wall_height], align = [0, -1, 1]);
            }



            // front_wall
            translate([0, -f_depth, 0]) {
                intersection() {
                    radiused_box(
                        inside_length = f_depth - plate_thickness * 2,
                        inside_width = f_width,
                        inside_height = front_wall_height,
                        inside_radius = corner_radius,
                        wall_thickness = plate_thickness,
                        align = [0, 1, 1]
                    );

                    color("red") mcube([f_width, corner_radius + plate_thickness, front_wall_height], align = [0, 1, 1]);

                }
            }


            // left wall
            translate([0, 0, 0]) color("orange") {
                hull() {
                    translate([-f_width / 2, manifold_overlap - (corner_radius + plate_thickness * 3/4), 0]) {
                        chamfered_box(dim = [plate_thickness, plate_thickness, back_wall_height], align = [1, -1, 1], chamfer = plate_thickness / 4);
                    }
                    translate([-f_width / 2, manifold_overlap - (f_depth - corner_radius - plate_thickness * 3/4), 0]) {
                        chamfered_box(dim = [plate_thickness, plate_thickness, front_wall_height], align = [1, 1, 1], chamfer = plate_thickness / 4);
                    }
                }
            }


            // right wall
            color("cyan") {
                hull() {
                    translate([f_width / 2, manifold_overlap - (corner_radius + plate_thickness * 3/4), 0]) {
                        chamfered_box(dim = [plate_thickness, plate_thickness, back_wall_height], align = [-1, -1, 1], chamfer = plate_thickness / 4);
                    }
                    translate([f_width / 2, manifold_overlap -(f_depth - corner_radius - plate_thickness * 3/4), 0]) {
                        chamfered_box(dim = [plate_thickness, plate_thickness, front_wall_height], align = [-1, 1, 1], chamfer = plate_thickness / 4);
                    }
                }
            }


            // floor
             color("fuchsia") hull() {
                radiused_box(
                    inside_length = f_depth - plate_thickness * 2 - manifold_overlap * 2,
                    inside_width = f_width - manifold_overlap * 2,
                    inside_height = plate_thickness,
                    inside_radius = corner_radius,
                    wall_thickness = plate_thickness,
                    align = [0, -1, 1]
                );
             }



            // module rail(height = 1, chamfer = 0, back_wall_gap = default_back_wall_gap, side_wall_gap = 0, copies = 1, mind_the_gap = true, skip = 0, topless = false) {
            if(rails) {
                color("orange") {
                    rotate([0, 0, 180]) {
                        rail(height = (back_wall_height / base_size), chamfer = plate_thickness / 4, copies = rails);
                    }
                }
            }
        }

        // crop top and bottom to compensate for closure errors
        color("red") union() {
            translate([0, 0, back_wall_height - manifold_overlap * 2]) {
                mcube([200, 200, 10], align = [0, 0, 1]);
            }
            translate([0, 0, manifold_overlap * 1]) {
                mcube([200, 200, 10], align = [0, 0, -1]);
            }
        }
    }
}

if(!is_undef(debug_slug)) {
    slug();
}


if(!is_undef(debug_model)) {
    tray(20, 40, 60, 80, 10, interior_dimensions = true);
}

